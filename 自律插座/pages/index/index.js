// index.js
Page({
  
  onLoad() {

  },
  onReady: function (e) {
    this.audioCtx = wx.createAudioContext('')
  },
  audioPlay: function () {
    this.audioCtx.play()
  },
  audioPause: function () {
    this.audioCtx.pause()
  },
  audioStart: function () {
    this.audioCtx.seek(0)
  }
})
